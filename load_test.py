from locust import HttpUser, TaskSet, task, between, events
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common import exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
import time
import warnings
import csv
import logging

USER_CREDENTIALS = None # Global Variable to Hold all User Credentials
NO_ITERATIONS = 1

class RealBrowserClient(object):

    def __init__(self, driver):
        self.driver = driver
        screen_width = 1200
        screen_height = 600
        wait_time_to_finish = 5
        set_window = True
        if set_window:
            self.driver.set_window_size(screen_width, screen_height)
        #self.wait = WebDriverWait(self.driver, wait_time_to_finish)

    def __getattr__(self, attr):
        return getattr(self.driver, attr)


class LocustHandler(HttpUser):

    wait_time = between(5,9) # Miniumum Waiting time between the execution of locust tasks
    host = "https://stonehunt.tntech.edu/guac/guacamole/#/"
    username = "NOT_FOUND"
    password = "NOT_FOUND"

    @events.init.add_listener
    def on_locust_init(**kwargs):

        # Prepare User Credentials
        global USER_CREDENTIALS # Ewwww Global Variables but oh well it works in this case since there's inherited threads used by Locust we don't have control over

        # For Testing Purposes
        # testing_users = ['user1', 'user2', 'user3', 'user4','user5','user7','user8','user9','user10','user11','user12','user13','user14','user15']
        # if (USER_CREDENTIALS == None):
        #     USER_CREDENTIALS = list()
        #     for user in testing_users[:5]:
        #         USER_CREDENTIALS.append([user, user])

        #For Real Thing
        if (USER_CREDENTIALS == None):
            with open('user_credentials.csv', 'r') as f:
                reader = csv.reader(f)
                USER_CREDENTIALS = list(reader)

    # Will be the first thing to run at the start of every task and this is where we can pop off the user credentials that we are testing for each 'thread'
    def on_start(self):
        if len(USER_CREDENTIALS) > 0:
            self.username, self.password = USER_CREDENTIALS.pop()

    def on_stop(self):
        # if len(USER_CREDENTIALS) == 0:
        #     global NO_ITERATIONS
        #     logging.info("\nDone with Iteration {}\n".format(NO_ITERATIONS))
        #     NO_ITERATIONS += 1
        pass

    # Task method that will handle all Locust Events automatically
    @task
    def start_load_test(self):

        # If number of total users that will be runing is small, uncomment below and comment out on_start() method to ensure that this user will just continue to execute with these tasks until USER_CREDENTIALS is empty
        # if len(USER_CREDENTIALS) > 0:
        #     self.username, self.password = USER_CREDENTIALS.pop()

        # Start Timer and Initialize Browser Client
        options = Options()
        options.headless = True
        start_time = time.time()
        self.client = RealBrowserClient(webdriver.Firefox(options = options , executable_path='geckodriver'))
        self.client.get(self.host) # Navigate to Website

        # Handle Assignment of Tasks
        logging.info("Running with {}".format(self.username))
        self.task_scheduler(self.username, self.password)
        logging.info("Done with {}".format(self.username))
        self.client.close() # Close WebDriver Connection

    # HELPER METHODS (MAIN METHODS)

    def task_scheduler(self, username, password):
        try:
            start_time = time.time()
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="GET", name="{}: Load Testing: GUAC Server Login Attempt".format(username), response_time=total_time, response_length=0)
            result = self.task_assignments(username, password)
        except Exception as event_exception:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="GET", name="{}: Load Testing: GUAC Server".format(username), response_time=total_time, exception=event_exception, response_length=0)
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="GET", name="{}: Load Testing: GUAC Server Login Successful: Holding for 5 Seconds".format(username), response_time=total_time, response_length=0)
            time.sleep(10)
            return result

    def task_assignments(self, username, password):

        try:
            # Make sure successful login, Successful Login to Guac Web Server to need to do more testing to sure actual login occured and no failures
            self.handle_login_page(username, password)
            self.check_for_no_guac_internal_error()
        except Exception as e:
            raise e

    # Handle Log on to Page
    def handle_login_page(self, username, password):

        # Explicitly Wait until Input Fields are visibile -> for simplicity, lets just do login_button and assume everything else is there if that is there
        try:
            login_button = WebDriverWait(self.client, 7).until(EC.element_to_be_clickable((By.CLASS_NAME, "login")))
        except:
            raise Exception("Timeout Trying to Get to Login Page")

        self.client.find_element_by_css_selector("input[type='text']").send_keys(username) # Send Username
        self.client.find_element_by_css_selector("input[type='password']").send_keys(password) # Send Password
        login_button.click()

        # Check to make sure login went through and there wasn't any failures and if so raise them
        if not self.check_connection_to_desktop():
            raise Exception("Failed During Login -> Perhaps Incorrect Username/Password, Username: {}".format(username))

    # Check if Connection to Actual Guac Server Succeeded -> Returns True if no error
    def check_connection_to_desktop(self):

        try:
            #print("Checking for Successful Login")
            guac_viewport = WebDriverWait(self.client, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "client-body"))) # Will Automatically time out if not found
            #print("Found Guac Client Body Visibility, Successful Login")
            return True
        except Exception as e:
            #print("Did Not Find Guac Client Visibility, Probably due to Incorrect User Credentials .. Passing Up Exception")
            return False

    # Check for Presence of Guac Internal Connection Error -> Returns True if no error
    def check_for_no_guac_internal_error(self):

        success = False
        time.sleep(1) # Give Page Time to Populate
        if self.client.find_element_by_css_selector("div[class='title ng-binding']").text == "CONNECTION ERROR":
            raise Exception("GUAC Internal Connection Error -> Perhaps Simulateneous Connection, Username: {}".format(self.client.username))

# class GuacTestUser(HttpUser):
#     wait_time = between(5, 9)
#     tasks = [LocustHandler]
#     host = "https://stonehunt.tntech.edu/guac/guacamole/#/"
